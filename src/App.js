import React, {useState} from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/Error';
import SpecificCourse from './components/SpecificCourse';
import {Container} from 'react-bootstrap'

import {UserProvider} from './UserContext';
//for routes
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';


// the Router{BrowserRouter} component will enable us to stimulate page navigation by synchronizing the shown content and the shown URL in the web browser
//The Routes (before it call switch) declares the ROute we can go to. For example, when we want to visit the courses page only.
function App() {
  
  //React context is nothung but a global state to tha app. It is a way to make a particular data available to all the components, no matter how they are nested. Context helps you to broadcast data and changes happening to that data/state to all components.


  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin')==='true'
 })

  //Function for clearing local storage on logout
  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <UserProvider value={{user,setUser,unsetUser}}>
      <Router>
       <AppNavbar />
        <Container>
       	<Routes>
          <Route path="/" element={<Home />} />
           <Route path="/courses" element={ <Courses />} />
           <Route path="/register" element={ <Register />} />
           <Route path="/login" element={ <Login />} />
          <Route path="/logout" element={ <Logout />} />
          <Route path="/courses/:courseId" element={<SpecificCourse />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </Container>
    </Router>
  </UserProvider>
  );
}

export default App;

