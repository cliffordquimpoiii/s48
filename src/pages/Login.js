import React, {useState, useEffect, useContext}from 'react';
//useContext is used to unpack or deconstruct the value of the UserContext
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';

export default function Login(){
	const navigate = useNavigate();
	//allows us to consume the User context object and it's properties to use for user validation
	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password1, setpassword1] = useState('');
	
	//State to determine whetehr submit button is enable or not in rendering
	const [isActive, setIsActive] = useState(true)

	useEffect(() =>{
		//Validation to Enavle submit button when all fields are populated and passwords match
		if(email !== '' && password1 !== ''){
			setIsActive(true);
		} 
		else
		{
			setIsActive(false);
		}
	}, [email, password1])

	/*Notes
		fetch() is a method in JS, which allows to send a request to an API and process it's response.

		Syntax:

		fetch('url', {options}).then(response => response.json()).then(data => {console.log(data)})
		'url = the url comming from the API/server
		-{optional object} = it contains additional information about our requests such as method, ody and headers (COntent-Type: application/json) or any other info.
		-then(response => response.json()) = parse the response as JSON
		-.then(data => {console.log(data)}) = process the actual data

	*/


	function loginUser(e) {
		e.preventDefault();

		fetch('https://b165-shared-api.herokuapp.com/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password1
			})
		})
		.then(response => response.json())
		.then(data => {

			console.log(data)
			if(data.accessToken !== undefined)
			{
				localStorage.setItem('accessToken', data.accessToken);
				setUser({accessToken: data.accessToken})
				Swal.fire({
					title: 'Yay',
					icon: 'success',
					text: 'Successfully Logged in'
				})
				//Getting the userCredentials
				fetch('https://b165-shared-api.herokuapp.com/getUserDetails', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				
				})
				.then( res => res.json())
				.then(result => {
					console.log(result)
					if(result.isAdmin === true){
						localStorage.setItem('email', result.email)
						localStorage.setItem('isAdmin', result.isAdmin)
						setUser({
							email: result.email,
							isAdmin: result.isAdmin
						})
						//redirect the admin to /courses
						navigate('/courses')

					}
					else //if not amin, redirect to home
					{
						navigate('/')
					}
				})
				
			}
			else if(data.message ==='User Not Found')
			{
				Swal.fire({
					title: 'Ooops!',
					icon: 'error',
					text: 'User Not Found'
				})
			}
			else{
				Swal.fire({
					title: 'Ooops!',
					icon: 'error',
					text: 'Somewthing Went Wrong. Check your Credentials'
				})

			}
			setEmail('');
			setpassword1('');
		})
	}
	return (
		//Create a conditional rendering state that will rediriect the user to the courses page when a user is logged in.
		(user.accessToken != null)?
		<Navigate to ="/courses" />
		:


		<Form className = "mt-3" onSubmit={(e)=> loginUser(e)}>
			<h1>Login</h1>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control type="email" 
				placeholder="Enter Email" 
				required 
				value={email}
				onChange={e => setEmail(e.target.value)} 
				/>
				<Form.Text className="text-muted">
					'Well never share your email with anyone else'
				</Form.Text>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
				type="password" 
				placeholder="Enter Password" 
				required
				value={password1}
				onChange={e => setpassword1(e.target.value)} 
				/>
				
			</Form.Group>
			{isActive ?
				<Button variant="primary" type="submit" className="mt-3">Login</Button>
				:
				<Button variant="primary" type="submit" className="mt-3" disabled>Login</Button>

			}
		</Form>
		)
}