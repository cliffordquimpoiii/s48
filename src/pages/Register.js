import React, {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
export default function Register(){
	const navigate = useNavigate();
	const {user } = useContext(UserContext);

	//State hooks to store the values of the input fields
	const [fName, setfName] = useState('');
	const [lName, setlName] = useState('');
	const [gendr, setGendr] = useState('');
	const [mobil, setMobil] = useState('');

	const [email, setEmail] = useState('');
	const [password1, setpassword1] = useState('');
	// const [password2, setpassword2] = useState('');
	//State to determine whetehr submit button is enable or not in rendering
	const [isActive, setIsActive] = useState(true)

	useEffect(() =>{
		//Validation to Enavle submit button when all fields are populated and passwords match
		
		if(email !== '' && password1 !== '' ){
			setIsActive(true);
		
		} 
		else
		{
			setIsActive(false);
		}
	}, [email, password1])

	function registerUser(e){
		//Prevents page redirection via form submission
		e.preventDefault();

		fetch('https://b165-shared-api.herokuapp.com/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({			
				firstName: fName,
				lastName: lName,
				email : email,
				password: password1,
				// passwordC: password2,
				gender: gendr,
				mobileNo: mobil
			})
		})
		.then(response => response.json())
		.then(data => {
			if (data === true){
				Swal.fire({
					title: 'Yay',
					icon: 'success',
					text: 'Successfully Registered'
				})

				navigate('/login')
			}
		
			else{	
					Swal.fire({
					title: 'Ooops!',
					icon: 'error',
					text: `${data.message}`
				})

					navigate('/register')

			}

		
	})
		setEmail('');
		setpassword1('');
		// setpassword2('');
}

	return (
		//Conditional rendering

		(user.accessToken !=null)?

		<Navigate to="/" />
		:
		<Form onSubmit={(e)=> registerUser(e)}>
			<h1>Register</h1>
			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control type="firstname" 
				placeholder="Enter First Name" 
				required 
				value={fName}
				onChange={e => setfName(e.target.value)} 
				/>
			</Form.Group>

				<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control type="lastName" 
				placeholder="Enter Last Name" 
				required 
				value={lName}
				onChange={e => setlName(e.target.value)} 
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control type="email" 
				placeholder="Enter Email" 
				required 
				value={email}
				onChange={e => setEmail(e.target.value)} 
				/>
				<Form.Text className="text-muted">
					'Well never share your email with anyone else'
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
				type="password" 
				placeholder="Enter Password" 
				required
				value={password1}
				onChange={e => setpassword1(e.target.value)} 
				/>
				
			</Form.Group>


			<Form.Group>
				<Form.Label> Gender</Form.Label>


				 { <Form.Select value={gendr} 
				 required
				 onChange={(e) => setGendr(e.target.value)}>


				      <option>Open this select menu</option>
				      <option value="male">Male</option>
				      <option value="female">Female</option>
				      <option value="idk">I Rather Not Say</option>
				   </Form.Select>}

			

			</Form.Group>

			<Form.Group>
				<Form.Label> Mobile Number </Form.Label>
				<Form.Control 
				type="mobile" 
				placeholder="Mobile Number" 
				required
				value={mobil} 
				onChange={e => setMobil(e.target.value)}
				/>
			</Form.Group>



			{isActive ?
				<Button variant="primary" type="submit" className="mt-3">Submit</Button>
				:
				<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>

			}
			
		</Form>
		)
}