import React, {useState, useEffect} from 'react';
import CourseCard from './Coursecard';

export default function UserView({coursesData}) {
	const [courses, setCourses] = useState([])
	useEffect(()=>{
		const coursesArr = coursesData.map(course =>{
			//only render the active courses
			if(course.isActive === true){
				return (
					<CourseCard courseProp={course} key={course._id} />
					)
			}
			else{
				return null;
			}
		})
		//set the courses state to the result to the result of our map function, to bring our returned course componend outside of the scope of our use effect where our return statement below can see.
		setCourses(coursesArr);
	}, [coursesData])	

	return (
		<>
			{courses}
		</>
		)
}